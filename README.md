## Rider Box

Aplikasi untuk driver (musti berjalan bareng dengan `client_box`)

###Connect 

endpoint : `http://localhost:9090/api/connect`

methode : `POST`

payload : `{
   "to" : "Joko",
   "password" : "123"
 }`
 
 ###Send Current Present 
 
 endpoint : `http://localhost:9090/api/present?to=Joko`
 
 methode : `GET`
 
 response : `{
                 "lon": 106.84513,
                 "lat": -6.21462
             }`
 
 ###Receive Request
 
 endpoint : `http://localhost:9090/api/request`
 
 methode : `GET`
 
 response : `{"from":"Ani","lon":120.84513,"lat":-5.21462}`
 
###Accept Request

 endpoint : `http://localhost:9090/api/accept`
 
 methode : `POST`
 
 payload : `{
              "status" : "accept",
              "to" : "Joko"
            }`
 
 response : `{
                 "successful": true
             }`
             
 ###Send Location
 
 endpoint : `http://localhost:9090/api/connect`
 
 methode : `POST`
 
 payload : `{
             "to":"Joko",
              "lon":120.84513,
              "lat":-5.21462
            }`
 
 response : `{
                    "successful": true
                }`
                
 ###Start Trip 
 
 endpoint : `http://localhost:9090/api/start`
 
 methode : `POST`
 
 payload : `{
              "from": "Ani",
               "to": "Joko"
            }`
            
 ###End Trip
 
 endpoint : `http://localhost:9090/api/end` 
 
 methode : `POST`
  
  payload : `{
               "from": "Ani",
                "to": "Joko"
             }`
             
 response : `{
            "from": "Ani",
                 "to": "Joko",
                 "distance" : 2.50,
                 "time" : "2018-11-05T13:37:27+00:00"
                 }`        
 
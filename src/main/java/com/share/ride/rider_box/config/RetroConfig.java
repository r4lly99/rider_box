package com.share.ride.rider_box.config;

import com.share.ride.rider_box.service.DriverServices;
import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Configuration
public class RetroConfig {

    @Bean
    public OkHttpClient client() {
        final OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        final OkHttpClient builtClient = okHttpClientBuilder.build();
        return builtClient;
    }

    @Bean
    public Retrofit retrofit(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl("http://localhost:8080")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Bean
    public DriverServices driverServices(Retrofit retrofit){
        return retrofit.create(DriverServices.class);
    }


}

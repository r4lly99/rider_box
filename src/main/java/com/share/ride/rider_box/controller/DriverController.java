package com.share.ride.rider_box.controller;


import com.google.gson.Gson;
import com.share.ride.rider_box.domain.*;
import com.share.ride.rider_box.service.CommonServices;
import com.share.ride.rider_box.service.DriverServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

@RestController
@RequestMapping("/api")
public class DriverController {

    @Autowired
    private DriverServices driverServices;

    @Autowired
    private CommonServices commonServices;

    private Connect connect;
    private SendRequest sendRequest;
    private AcceptRequest acceptRequest;

    private static final Logger logger = LoggerFactory.getLogger(DriverController.class);

    private boolean LOGIN_STATUS = false ;

    @RequestMapping(value = "/receive" , method = RequestMethod.POST)
    public ResponseEntity receiveRequest(@RequestBody SendRequest request){
        Gson gson = new Gson();
        logger.info("Receive Request : {}", gson.toJson(request));
        sendRequest = request;
        if (sendRequest.equals(request)){
            return ResponseEntity.ok(request);
        }else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(request);
        }
    }

    @RequestMapping(value = "/request" , method = RequestMethod.GET)
    public ResponseEntity getRequest() throws IOException {
        Gson gson = new Gson();
        logger.info("Request : {}", gson.toJson(sendRequest));
        return ResponseEntity.ok(sendRequest);
    }

    @RequestMapping(value = "/accept" , method = RequestMethod.POST)
    public ResponseEntity acceptRequest(@RequestBody AcceptRequest request ) throws IOException {
        Gson gson = new Gson();
        logger.info("Request to Accept : {}", gson.toJson(sendRequest));
        acceptRequest = new AcceptRequest();
        acceptRequest.setStatus(request.getStatus());
        acceptRequest.setLat(sendRequest.getLat());
        acceptRequest.setLon(sendRequest.getLon());
        acceptRequest.setFrom(sendRequest.getFrom());
        acceptRequest.setTo(request.getTo());
        logger.info("Accept Request : {}", gson.toJson(acceptRequest));
        Call<AcceptRequest> requestCall = driverServices.acceptRequest(acceptRequest);
        Response<AcceptRequest> response = requestCall.execute();
        if(response.isSuccessful()){
            return ResponseEntity.ok(response);
        }else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response.body());
        }
    }

    @RequestMapping(value = "/connect" , method = RequestMethod.POST)
    public ResponseEntity connect(@RequestBody Connect request){
        Gson gson = new Gson();
        logger.info("Login Info : {}", gson.toJson(request));
        LOGIN_STATUS = commonServices.getConnection(request.getTo(), request.getPassword());
        if(LOGIN_STATUS){
            connect = request ;
            return ResponseEntity.ok("connect success");
        }else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(request);
        }

    }

    @RequestMapping(value = "/present" , method = RequestMethod.GET)
    public ResponseEntity sendCurrentPresent(@RequestParam("to") String to) {
        Gson gson = new Gson();
        logger.info("Present Info : {}", gson.toJson(connect));
        if(LOGIN_STATUS && connect.getTo().contentEquals(to)) {
            CurrentPresent currentPresent = commonServices.sendCurrentPresence(to);
            logger.info("Present Status : {}", gson.toJson(currentPresent));
            return ResponseEntity.ok(currentPresent);
        }else{
            logger.info("Present Info Failed : {}", gson.toJson(to));
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(to);
        }
    }

    @RequestMapping(value = "/send" , method = RequestMethod.POST)
    public ResponseEntity sendLocation(@RequestBody SendLocation send) throws IOException {
        Gson gson = new Gson();
        logger.info("Login Info : {}", gson.toJson(send));
        Call<SendLocation> requestCall = driverServices.sendLocation(send);
        Response<SendLocation> response = requestCall.execute();
        if(response.isSuccessful()){
            return ResponseEntity.ok(response);
        }else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response.body());
        }

    }

    @RequestMapping(value = "/start" , method = RequestMethod.POST)
    public ResponseEntity startTrip(@RequestBody Trip trip) {
        Gson gson = new Gson();
        logger.info("Start : {}", gson.toJson(trip));
        if(trip.getFrom().contentEquals(sendRequest.getFrom())){
            ResponseEntity.ok(trip);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(trip);
    }

    @RequestMapping(value = "/end" , method = RequestMethod.POST)
    public ResponseEntity endTrip(@RequestBody Trip trip) {
        Gson gson = new Gson();
        logger.info("Start : {}", gson.toJson(trip));
        Trip endTrip = commonServices.getTripDistanceTime(trip.getFrom());
        logger.info("End : {}", gson.toJson(endTrip));
        if(trip.getTo().contentEquals(acceptRequest.getTo())){
            ResponseEntity.ok(endTrip);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(endTrip);
    }



}


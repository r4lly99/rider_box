package com.share.ride.rider_box.service;

import com.share.ride.rider_box.domain.AcceptRequest;
import com.share.ride.rider_box.domain.SendLocation;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface DriverServices {

    @POST("api/post")
    Call<AcceptRequest> acceptRequest(@Body AcceptRequest request) ;

    @POST("api/send")
    Call<SendLocation> sendLocation(@Body SendLocation send) ;
}

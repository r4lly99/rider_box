package com.share.ride.rider_box.service;

import com.share.ride.rider_box.domain.CurrentPresent;
import com.share.ride.rider_box.domain.Trip;

public interface CommonServices {

    boolean getConnection(String userName, String password) ;

    CurrentPresent sendCurrentPresence(String userName) ;

    Trip getTripDistanceTime(String userName);

}

package com.share.ride.rider_box.service;

import com.share.ride.rider_box.domain.CurrentPresent;
import com.share.ride.rider_box.domain.Trip;
import org.springframework.stereotype.Service;

@Service
public class CommonServicesImpl implements CommonServices {

    @Override
    public boolean getConnection(String driverName, String password) {
        if(driverName.contentEquals("Joko") && password.contentEquals("123")){
            return true;
        }
        return false;
    }

    @Override
    public CurrentPresent sendCurrentPresence(String userName) {
        return new CurrentPresent(120.84513, -5.21462);
    }

    @Override
    public Trip getTripDistanceTime(String userName) {
        Trip trip = new Trip();
        trip.setFrom(userName);
        trip.setDistance(2.50);
        trip.setTime("2018-11-05T13:37:27+00:00");
        return trip;
    }


}

package com.share.ride.rider_box;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RiderBoxApplication {

	public static void main(String[] args) {
		SpringApplication.run(RiderBoxApplication.class, args);
	}
}
